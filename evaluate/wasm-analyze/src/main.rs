mod analyze;

use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {

    #[arg(short = 'f', long)]
    file: String,
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();

    env_logger::init();

    let wasm_info = analyze::analyze(&args.file, analyze::default_wasm_config())?;
    let result = serde_json::to_string_pretty(&wasm_info)?;
    println!("{}", result);

    Ok(())
}
