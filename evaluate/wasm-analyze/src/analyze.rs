use std::collections::HashSet;

use anyhow::{Context, Error};
use serde::Serialize;
use wasmparser::WasmFeatures;
use wasmtime::{ExportType, ExternType, ImportType, ResourcesRequired};
use wasmtime::component::__internal::wasmtime_environ;
use wasmtime::component::__internal::wasmtime_environ::{ScopeVec, Tunables};
use wasmtime::component::__internal::wasmtime_environ::component::Translator;

#[derive(Serialize)]
pub struct WasmInfo {
    pub name: Option<String>,
    pub is_core_wasm: bool,
    pub is_component: bool,
    pub module: Vec<WasmModule>,
    pub component: Option<wasmtime_environ::component::Component>
}

#[derive(Clone,Serialize)]
pub struct WasmModule {
    pub name: Option<String>,
    pub wasi: Option<WasmWasi>,
    pub exports: Vec<WasmExport>,
    pub imports: Vec<WasmImport>,
    pub resources_required: WasmResourcesRequired
}

#[derive(Clone,Serialize)]
pub struct WasmWasi {
    pub specs: Vec<String>,
    pub kind: Option<String>
}

#[derive(Clone,Serialize)]
pub struct WasmExport {
    pub name: String,
    pub kind: String,
    pub definition: String
}

#[derive(Clone,Serialize)]
pub struct WasmImport {
    pub module: String,
    pub name: String,
    pub kind: String,
    pub definition: String
}

#[derive(Clone,Serialize)]
pub struct WasmResourcesRequired {
    pub num_memories: u32,
    pub max_initial_memory_size: Option<u64>,
    pub num_tables: u32,
    pub max_initial_table_size: Option<u32>,
}

pub fn default_wasm_config() -> wasmtime::Config {
    wasmtime::Config::default()
        .strategy(wasmtime::Strategy::Cranelift)
        .wasm_component_model(true)
        .wasm_function_references(true)
        .wasm_tail_call(true)
        .to_owned()
}

pub fn analyze(wasm_file: &str, config: wasmtime::Config) -> anyhow::Result<WasmInfo> {
    let wasm_bytes = std::fs::read(&wasm_file)?;

    let is_core_wasm = wasmparser::Parser::is_core_wasm(&wasm_bytes);
    let is_component = wasmparser::Parser::is_component(&wasm_bytes);

    let engine =  wasmtime::Engine::new(&config)?;
    if is_core_wasm && !is_component {
        let wasm_module = analyze_module(&wasm_bytes, &engine)?;
        let name = (&wasm_module.name).to_owned();
        Ok(WasmInfo {
            name,
            is_core_wasm,
            is_component,
            module: vec![wasm_module],
            component: None,
        })
    } else if is_component {
        let (component, wasm_modules) = analyze_component(&wasm_bytes, &engine)?;
        Ok(WasmInfo {
            name: None,
            is_core_wasm,
            is_component,
            module: wasm_modules,
            component: Some(component),
        })
    } else {
        Err(Error::msg("Unknown type"))
    }
}

pub fn analyze_module(wasm_bytes: &[u8], engine: &wasmtime::Engine) -> anyhow::Result<WasmModule> {
    let wasm_module = wasmtime::Module::from_binary(&engine, &wasm_bytes)?;

    let exports: Vec<WasmExport> = wasm_module.exports().map(|v|to_wasm_export(v)).collect();
    let imports: Vec<WasmImport> = wasm_module.imports().map(|v|to_wasm_import(v)).collect();

    Ok(WasmModule {
        name: wasm_module.name().map(|v|v.to_string()),
        wasi: to_specs_wasm_wasi(&imports, &exports),
        exports,
        imports,
        resources_required: to_wasm_resources_required(wasm_module.resources_required())
    })
}

fn analyze_component(wasm_bytes: &[u8], engine: &wasmtime::Engine) -> anyhow::Result<(wasmtime_environ::component::Component,Vec<WasmModule>)> {
    let scope = ScopeVec::new();
    let mut validator = wasmparser::Validator::new_with_features(WasmFeatures::default());

    let mut types = Default::default();
    let (component, module_translations) =
        Translator::new(&Tunables::default_host(), &mut validator, &mut types, &scope)
            .translate(wasm_bytes)
            .context("failed to parse WebAssembly module")?;
    let wasm_modules = module_translations.values()
        .map(|m|analyze_module(m.wasm, engine).unwrap())
        .collect::<Vec<WasmModule>>();
    Ok((component.component, wasm_modules))
}

fn to_wasm_export(export_type: ExportType) -> WasmExport {
    WasmExport {
        name: export_type.name().to_string(),
        kind: extern_type_to_kind(export_type.ty()),
        definition: extern_type_to_definition(export_type.ty())
    }
}

fn to_wasm_import(import_type: ImportType) -> WasmImport {
    WasmImport {
        module: import_type.module().to_string(),
        name: import_type.name().to_string(),
        kind: extern_type_to_kind(import_type.ty()),
        definition: extern_type_to_definition(import_type.ty())
    }
}

fn to_specs_wasm_wasi(wasm_imports: &Vec<WasmImport>, wasm_exports: &Vec<WasmExport>) -> Option<WasmWasi> {
    let wasi_specs: Vec<String> = wasm_imports.iter()
        .map(|i|i.module.to_string())
        .filter(|m| {m.eq("wasi") || m.contains("wasi_") || m.contains("wasip")})
        .collect::<HashSet<String>>().into_iter().collect::<Vec<String>>();
    if wasi_specs.is_empty() {
        None
    } else {
        Some(WasmWasi {
            specs: wasi_specs,
            kind: to_wasi_kind(&wasm_imports, &wasm_exports)
        })
    }
}

fn to_wasm_resources_required(resources_required: ResourcesRequired) -> WasmResourcesRequired {
    WasmResourcesRequired {
        num_memories: resources_required.num_memories,
        max_initial_memory_size: resources_required.max_initial_memory_size,
        num_tables: resources_required.num_tables,
        max_initial_table_size: resources_required.max_initial_table_size,
    }
}

fn extern_type_to_kind(extern_type: ExternType) -> String {
    match extern_type {
        ExternType::Func(_) => {"Func".to_string()}
        ExternType::Global(_) => {"Global".to_string()}
        ExternType::Table(_) => {"Table".to_string()}
        ExternType::Memory(_) => {"Memory".to_string()}
    }
}

fn extern_type_to_definition(extern_type: ExternType) -> String {
    match extern_type {
        ExternType::Func(func) => {
            format!("{}",func)
        }
        ExternType::Global(global) => {
            format!("(type (global (content ({:?} {}))))",
                    global.mutability(), global.content())
        }
        ExternType::Table(table) => {
            format!("(type (table (element {} (min {}) (max {}))))",
                    table.element(), table.minimum(), table.maximum().map_or("None".to_string(), |i|i.to_string()))
        }
        ExternType::Memory(memory) => {
            format!("(type (memory (min {}) (max {})))",
                    memory.minimum(), memory.maximum().map_or("None".to_string(), |i|i.to_string()))
        }
    }
}

fn to_wasi_kind(wasm_imports: &Vec<WasmImport>, wasm_exports: &Vec<WasmExport>) -> Option<String> {
    if is_wasi_preview0(&wasm_imports) || is_wasi_preview1(&wasm_imports) {
        if has_exported_function(wasm_exports, "_start") {
            Some("command".to_string())
        } else {
            Some("reactor".to_string())
        }
    } else if is_wasi_preview2(&wasm_imports) {
        Some("component".to_string())
    } else {
        None
    }
}

fn is_wasi_preview0(wasm_imports: &Vec<WasmImport>) -> bool {
    !wasm_imports.iter()
        .map(|i|i.module.to_string())
        .filter(|m| {m.eq("wasi_unstable") || m.eq("snapshot_0")})
        .collect::<Vec<String>>()
        .is_empty()
}

fn is_wasi_preview1(wasm_imports: &Vec<WasmImport>) -> bool {
    !wasm_imports.iter()
        .map(|i|i.module.to_string())
        .filter(|m| {m.eq("wasi_snapshot_preview1") || m.eq("wasip1")})
        .collect::<Vec<String>>()
        .is_empty()
}

fn is_wasi_preview2(wasm_imports: &Vec<WasmImport>) -> bool {
    !wasm_imports.iter()
        .map(|i|i.module.to_string())
        .filter(|m| {m.eq("wasi_snapshot_preview2") || m.eq("wasip2")})
        .collect::<Vec<String>>()
        .is_empty()
}

fn has_exported_function(wasm_exports: &Vec<WasmExport>, func: &str) -> bool {
    !wasm_exports.iter()
        .filter(|e|e.kind.eq("Func"))
        .map(|i|i.name.to_string())
        .filter(|n|n.eq(func))
        .collect::<Vec<String>>()
        .is_empty()
}

