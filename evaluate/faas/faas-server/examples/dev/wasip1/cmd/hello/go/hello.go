package main

import (
	"fmt"
	"os"
	"bufio"
)

func main() {
  fmt.Println("content-type: text/plain")
  fmt.Println("")

  fmt.Println("Hello Go!")
  fmt.Println("")

  fmt.Println("==== Args: ====")
  for _, arg := range os.Args {
    fmt.Println(arg)
  }

  fmt.Println("==== Environment: ====")
  for _, env := range os.Environ() {
    fmt.Println(env)
  }

  fmt.Println("==== Stdin: ====")
	if fileInfo, _ := os.Stdin.Stat(); (fileInfo.Mode() & os.ModeCharDevice) == 0 {
    scanner := bufio.NewScanner(os.Stdin)
    for scanner.Scan() {
      fmt.Println(scanner.Text())
		}
	}
}
