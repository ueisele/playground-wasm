use std::path::Path;
use std::sync::{Arc, RwLock};

use anyhow::{bail, Error, Result};
use log::{info, warn};
use notify::{Event, EventHandler, EventKind, INotifyWatcher, RecursiveMode, Watcher};
use notify::event::{AccessKind, AccessMode, DataChange, ModifyKind, RemoveKind, RenameMode};
use walkdir::WalkDir;

use crate::fnmanager::FnRegistrar;

pub trait FnObserver {
    fn start(&self, registrar: impl FnRegistrar + Sync + Send + 'static) -> Result<(), Error>;
    fn stop(&self) -> Result<(), Error>;
}

#[derive(Clone)]
pub struct FileSystemFnObserver {
    base_dir: String,
    recursive_mode: RecursiveMode,
    watcher: Arc<RwLock<Option<INotifyWatcher>>>
}

impl FileSystemFnObserver {
    pub fn new(base_dir: &str, recursive_mode: RecursiveMode) -> Result<Self, Error>  {
        Ok(FileSystemFnObserver {
            base_dir: std::path::absolute(Path::new(base_dir))?.to_str().unwrap().to_string(),
            recursive_mode,
            watcher: Arc::new(RwLock::new(None))
        })
    }

    fn start_watcher(event_handler: FSEventHandler, recursive_mode: RecursiveMode) -> Result<INotifyWatcher, Error> {
        let mut watcher = notify::recommended_watcher(event_handler.clone())?;
        info!("Starts watching for functions in path {}", event_handler.base_dir);
        watcher.watch(Path::new(&event_handler.base_dir), recursive_mode)?;
        Ok(watcher)
    }

    fn handle_existing_files(event_handler: FSEventHandler, recursive_mode: RecursiveMode) {
        WalkDir::new(Path::new(&event_handler.base_dir.to_string()))
            .follow_links(false).follow_root_links(false)
            .max_depth(if recursive_mode.eq(&RecursiveMode::Recursive) {usize::MAX} else {0})
            .into_iter()
            .filter_map(|r| r.ok())
            .filter(|e| e.metadata().map(|m| m.is_file()).unwrap_or(false))
            .for_each(|f| event_handler.on_added(f.path()))
    }
}

impl FnObserver for FileSystemFnObserver {
    fn start(&self, registrar: impl FnRegistrar + Sync + Send + 'static) -> Result<(), Error> {
        let mut maybe_watcher = self.watcher.write().unwrap();
        match maybe_watcher.as_mut() {
            None => {
                let event_handler = FSEventHandler::new(&self.base_dir, registrar);
                let _ = maybe_watcher.insert(FileSystemFnObserver::start_watcher(event_handler.clone(), self.recursive_mode)?);
                FileSystemFnObserver::handle_existing_files(event_handler, self.recursive_mode);
            }
            Some(_) => bail!("observer has already been started")
        }
        Ok(())
    }

    fn stop(&self) -> Result<(), Error> {
        match self.watcher.write().unwrap().take().as_mut() {
            None => {}
            Some(watcher) => {
                watcher.unwatch(Path::new(&self.base_dir))?;
            }
        }
        Ok(())
    }

}

impl Drop for FileSystemFnObserver {
    fn drop(&mut self) {
        self.stop().ok();
    }
}

#[derive(Clone)]
struct FSEventHandler {
    base_dir: String,
    registrar: Arc<dyn FnRegistrar + Sync + Send + 'static>
}

impl FSEventHandler {
    fn new(base_dir: &str, registrar: impl FnRegistrar + Send + Sync + 'static) -> Self {
        FSEventHandler {
            base_dir: base_dir.to_string(),
            registrar: Arc::new(registrar),
        }
    }

    fn on_added(&self, path: &Path) {
        match path.extension().map_or("", |p| p.to_str().unwrap()) {
            "wasm" | "wat" => {
                let name = self.path_to_name(path);
                match &std::fs::read(&path) {
                    Ok(bytes) => match self.registrar.register(&name, bytes) {
                        Ok(_) => info!("Successfully registered function: {name}"),
                        Err(err) => warn!("Failed to register function '{name}': {err}")
                    },
                    Err(err) => warn!("Failed to read function '{name}' from file '{}': {err}", path.display())
                }
            }
            _ => {}
        }
    }

    fn on_removed(&self, path: &Path) {
        let name = self.path_to_name(path);
        self.registrar.unregister(&name);
        info!("Successfully un-registered function: {name}");
    }

    fn path_to_name(&self, path: &Path) -> String {
        pathdiff::diff_paths(path, &Path::new(&self.base_dir)).unwrap().with_extension("").to_str().unwrap().to_string()
    }
}

impl EventHandler for FSEventHandler {
    fn handle_event(&mut self, event: notify::Result<Event>) {
        match event {
            Ok(Event {kind: EventKind::Access(AccessKind::Close(AccessMode::Write)), paths, attrs: _attrs }) => self.on_added(paths.get(0).unwrap()),
            Ok(Event {kind: EventKind::Modify(ModifyKind::Data(DataChange::Any)), paths, attrs: _attrs }) => self.on_added(paths.get(0).unwrap()),
            Ok(Event {kind: EventKind::Modify(ModifyKind::Name(RenameMode::To)), paths, attrs: _attrs }) => self.on_added(paths.get(0).unwrap()),
            Ok(Event {kind: EventKind::Modify(ModifyKind::Name(RenameMode::From)), paths, attrs: _attrs }) => self.on_removed(paths.get(0).unwrap()),
            Ok(Event {kind: EventKind::Remove(RemoveKind::File), paths, attrs: _attrs }) => self.on_removed(paths.get(0).unwrap()),
            _ => {}
        }
    }
}