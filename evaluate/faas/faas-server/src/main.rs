use anyhow::{Error, Result};
use clap::Parser;
use log::info;
use notify::RecursiveMode;

use faas_server::{fnmanager, fnobserver, fnserver};
use faas_server::fnobserver::FnObserver;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {

    #[arg(short = 'd', long)]
    fn_base_dir: String,

    #[arg(short = 'i', long, default_value = "0.0.0.0")]
    http_listener_ip: String,


    #[arg(short = 'p', long, default_value_t = 8080)]
    http_listener_port: u16,


    #[arg(short = 'b', long, default_value = "/api/fn")]
    http_base_path: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Args::parse();

    env_logger::init();
    info!("Starting...");

    let fn_manager = fnmanager::FnManager::default();
    let fn_observer = fnobserver::FileSystemFnObserver::new(&args.fn_base_dir, RecursiveMode::Recursive)?;
    fn_observer.start(fn_manager.clone())?;
    let fn_server = fnserver::HttpFnServer::new(&args.http_listener_ip, args.http_listener_port, &args.http_base_path, fn_manager);

    fn_server.start().await
}
