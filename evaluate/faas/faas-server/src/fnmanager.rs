use std::collections::HashMap;
use std::sync::{Arc, Mutex, RwLock};

use anyhow::{Error, Result};
use hyper::body::Bytes;

pub fn default_wasm_config() -> wasmtime::Config {
    wasmtime::Config::default()
        .strategy(wasmtime::Strategy::Cranelift)
        .cranelift_pcc(false)
        .wasm_function_references(true)
        .wasm_tail_call(true)
        .allocation_strategy(wasmtime::InstanceAllocationStrategy::pooling())
        .to_owned()
}

pub trait FnRegistrar {
    fn register(&self, name: &str, bytes: &[u8]) -> Result<(), Error>;
    fn unregister(&self, name: &str);
}

pub trait ModuleFnRunner {
    fn run_module(&self, name: &str, args: &[(impl AsRef<str> + ToString)], envs: &[((impl AsRef<str> + ToString), (impl AsRef<str> + ToString))], stdin: impl Into<Bytes>) -> Result<Option<String>, Error>;

    fn list_modules(&self) -> Vec<String>;
}

#[derive(Clone)]
pub struct FnManager {
    engine: wasmtime::Engine,
    modules: Arc<RwLock<HashMap<String,FnModule>>>
}

impl FnManager {
    pub fn new(wasm_config: &wasmtime::Config) -> Result<FnManager, Error> {
        Ok(FnManager {
            engine: wasmtime::Engine::new(&wasm_config)?,
            modules: Arc::new(RwLock::new(HashMap::new()))
        })
    }
}

impl Default for FnManager {
    fn default() -> Self {
        return FnManager::new(&default_wasm_config()).unwrap();
    }
}

impl FnRegistrar for FnManager {
    fn register(&self, name: &str, bytes: &[u8]) -> Result<(), Error> {
        let mut m = self.modules.write().unwrap();
        if !wasmparser::Parser::is_core_wasm(bytes) {
            return Err(Error::msg(format!("'{name}' is not a core wasm module!")));
        }
        m.insert(name.to_string(), FnModule::new(&self.engine, name, &bytes, 1024 * 1024)?);
        Ok(())
    }

    fn unregister(&self, name: &str) {
        let mut m = self.modules.write().unwrap();
        m.remove(name);
    }
}

impl ModuleFnRunner for FnManager {
    fn run_module(&self, module_name: &str, args: &[(impl AsRef<str> + ToString)], envs: &[((impl AsRef<str> + ToString), (impl AsRef<str> + ToString))], stdin: impl Into<Bytes>) -> Result<Option<String>, Error> {
        let m = self.modules.read().unwrap();
        match m.get(module_name) {
            None => Ok(None),
            Some(fn_module) => fn_module.run(args, envs, stdin).map(|r| Some(r))
        }
    }

    fn list_modules(&self) -> Vec<String> {
        self.modules.read().unwrap().keys().map(|k| k.to_string()).collect()
    }
}

#[derive(Clone)]
struct FnModule {
    name: String,
    module: wasmtime::Module,
    linker: wasmtime::Linker<Host>,
    output_max_bytes: usize
}

impl FnModule {
    pub fn new(engine: &wasmtime::Engine, name: &str, bytes: &[u8], output_max_bytes: usize) -> Result<FnModule, Error> {
        // init module
        let fn_module = FnModule {
            name: name.to_string(),
            module: wasmtime::Module::from_binary(engine, bytes)?,
            linker: Self::init_linker(engine)?,
            output_max_bytes
        };
        // check if it is a core wasm command module (_start function)
        // https://dylibso.com/blog/wasi-command-reactor/
        let mut store = fn_module.init_store(&Vec::<String>::new(), &Vec::<(String, String)>::new(), wasmtime_wasi::pipe::MemoryInputPipe::new(Vec::<u8>::new()), wasmtime_wasi::pipe::SinkOutputStream{})?;
        fn_module.init_func(&mut store)?;
        // return module
        Ok(fn_module)
    }

    pub fn run(&self, args: &[(impl AsRef<str> + ToString)], envs: &[((impl AsRef<str> + ToString), (impl AsRef<str> + ToString))], stdin: impl Into<Bytes>) -> Result<String, Error> {
        let stdin = wasmtime_wasi::pipe::MemoryInputPipe::new(stdin);
        let stdout = wasmtime_wasi::pipe::MemoryOutputPipe::new(self.output_max_bytes);
        let mut store = self.init_store(args, envs, stdin.clone(), stdout.clone())?;
        let func = self.init_func(&mut store)?;
        func
            .typed::<(), ()>(&mut store)?
            .call(&mut store, ())?;
        let output = String::from_utf8(stdout.contents().to_vec())?;
        Ok(output)
    }

    fn init_linker(engine: &wasmtime::Engine) -> Result<wasmtime::Linker<Host>, Error> {
        let mut linker: wasmtime::Linker<Host> = wasmtime::Linker::new(&engine);
        wasmtime_wasi::preview0::add_to_linker_sync(&mut linker, |t| t)?;
        wasmtime_wasi::preview1::add_to_linker_sync(&mut linker, |t| t)?;
        Ok(linker)
    }

    fn init_store(&self, args: &[(impl AsRef<str> + ToString)], envs: &[((impl AsRef<str> + ToString), (impl AsRef<str> + ToString))],
                  stdin: impl wasmtime_wasi::StdinStream + 'static, stdout: impl wasmtime_wasi::StdoutStream + 'static) -> Result<wasmtime::Store<Host>, Error> {
        let wasi = wasmtime_wasi::WasiCtxBuilder::new()
            .args(&self.compute_module_args(args))
            .envs(envs)
            .stdin(stdin)
            .stdout(stdout)
            .build();
        let host = Host {
            preview2_ctx: Some(Arc::new(Mutex::new(wasi))),
            preview2_table: Arc::new(Mutex::new(wasmtime::component::ResourceTable::default())),
            preview2_adapter: Arc::new(wasmtime_wasi::preview1::WasiPreview1Adapter::default())
        };
        Ok(wasmtime::Store::new(&self.linker.engine(), host))
    }

    fn init_func(&self, mut store: impl wasmtime::AsContextMut<Data = Host>) -> Result<wasmtime::Func, Error> {
        let instance = self.linker.instantiate(&mut store, &self.module)?;
        instance
            .get_func(&mut store, "_start")
            .ok_or(Error::msg(format!("'_start' not found in module {}", self.name)))
    }

    fn compute_module_args(&self, args: &[(impl AsRef<str> + ToString)]) -> Vec<String> {
        let mut module_args = vec![self.name.to_string()];
        module_args.extend(args.iter().map(|o| o.to_string()));
        module_args
    }
}

#[derive(Default, Clone)]
struct Host {
    // The Mutex is only needed to satisfy the Sync constraint but we never
    // actually perform any locking on it as we use Mutex::get_mut for every
    // access.
    preview2_ctx: Option<Arc<Mutex<wasmtime_wasi::WasiCtx>>>,

    // Resource table for preview2 if the `preview2_ctx` is in use, otherwise
    // "just" an empty table.
    preview2_table: Arc<Mutex<wasmtime::component::ResourceTable>>,

    // State necessary for the preview1 implementation of WASI backed by the
    // preview2 host implementation.
    preview2_adapter: Arc<wasmtime_wasi::preview1::WasiPreview1Adapter>,
}

impl wasmtime_wasi::WasiView for Host {
    fn table(&mut self) -> &mut wasmtime::component::ResourceTable {
        Arc::get_mut(&mut self.preview2_table)
            .unwrap()
            .get_mut()
            .unwrap()
    }

    fn ctx(&mut self) -> &mut wasmtime_wasi::WasiCtx {
        let ctx = self.preview2_ctx.as_mut().unwrap();
        Arc::get_mut(ctx)
            .unwrap()
            .get_mut()
            .unwrap()
    }
}

impl wasmtime_wasi::preview1::WasiPreview1View for Host {
    fn adapter(&self) -> &wasmtime_wasi::preview1::WasiPreview1Adapter {
        &self.preview2_adapter
    }

    fn adapter_mut(&mut self) -> &mut wasmtime_wasi::preview1::WasiPreview1Adapter {
        Arc::get_mut(&mut self.preview2_adapter).unwrap()
    }
}
