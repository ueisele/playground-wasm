use std::net::SocketAddr;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use anyhow::{Context, Error};
use bytes::Bytes;
use http::header;
use http_body_util::{BodyExt, Full};
use hyper::{HeaderMap, http, Method, Request, Response, StatusCode};
use hyper::body::Incoming;
use hyper::http::HeaderValue;
use hyper::server::conn::http1;
use hyper::service::service_fn;
use hyper_util::rt::TokioIo;
use log::{info, warn};
use regex::Regex;
use tokio::net::{TcpListener, TcpStream};

use crate::fnmanager::{FnManager, ModuleFnRunner};

#[derive(Clone)]
pub struct HttpFnServer {
    host: String,
    port: u16,
    fn_http_service: FnHttpService,
    should_stop: Arc<AtomicBool>
}

impl HttpFnServer {

    pub fn new(host: &str, port: u16, base_path: &str, fn_manager: FnManager) -> HttpFnServer {
        HttpFnServer {
            host: host.to_string(),
            port,
            fn_http_service: FnHttpService::new(base_path, fn_manager),
            should_stop: Arc::new(AtomicBool::new(false))
        }
    }

    pub async fn start(&self) -> anyhow::Result<(), Error> {
        let addr: SocketAddr = format!("{}:{}",self.host,self.port).parse().unwrap();
        let listener = TcpListener::bind(&addr).await?;
        info!("Listening on http://{}", addr);

        while !self.should_stop.load(Ordering::Relaxed) {
            let (stream, _) = listener.accept().await?;
            tokio::task::spawn(HttpFnServer::handle_request(TokioIo::new(stream), self.fn_http_service.clone()));
        }

        Ok(())
    }

    pub fn stop(&self) {
        self.should_stop.store(true, Ordering::Relaxed);
    }

    async fn handle_request(io: TokioIo<TcpStream>, fn_http_service: FnHttpService) {
        let service = service_fn(|req| async {
            match fn_http_service.call(req).await.context("error in http handler") {
                Err(err) => Ok(Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .header(header::CONTENT_TYPE, "text/plain")
                    .body(Full::from(format!("{:?}", err))).unwrap()),
                response => response
            }
        });
        if let Err(err) = http1::Builder::new().serve_connection(io, service).await {
            warn!("Failed to serve connection: {:?}", err);
        }
    }

}

#[derive(Clone)]
struct FnHttpService {
    base_path: String,
    fn_manager: Arc<FnManager>
}

impl FnHttpService {
    pub fn new(base_path: &str, fn_manager: FnManager) -> FnHttpService {
        FnHttpService {
            base_path: base_path.to_string(),
            fn_manager: Arc::new(fn_manager)
        }
    }

    async fn call(&self, req: Request<Incoming>) -> Result<Response<Full<Bytes>>, Error> {
        let method = req.method();
        let path = req.uri().path().to_string();
        if method.eq(&Method::GET) && path.eq(&self.base_path) {
            return self.list_fn().await;
        }
        let path_re = Regex::new(format!(r"{}/(?P<name>.+)",&self.base_path).as_str()).unwrap();
        match path_re.captures(&path) {
            Some(cap) => {self.call_fn(cap.name("name").unwrap().as_str(), req).await}
            None => Ok(Response::builder().status(StatusCode::NOT_FOUND).body(Full::default()).unwrap())
        }
    }

    async fn list_fn(&self) -> Result<Response<Full<Bytes>>, Error> {
        let json = serde_json::to_string_pretty(&self.fn_manager.list_modules()).unwrap();
        Ok(Response::builder().status(StatusCode::OK).header(header::CONTENT_TYPE, "application/json").body(Full::from(json)).unwrap())
    }

    async fn call_fn(&self, name: &str, req: Request<Incoming>) -> Result<Response<Full<Bytes>>, Error> {
        let args: Vec<String> = req.uri().query().map_or(vec![], |q| q.split("&").map(|s| s.to_string()).collect());
        let mut envs: Vec<(String,String)> = vec![
            ("X_VERSION".to_string(), format!("{:?}",req.version())),
            ("X_METHOD".to_string(), req.method().to_string()),
            ("X_URI_PATH_FULL".to_string(), req.uri().path().to_string()),
            ("X_URI_PATH_BASE".to_string(), format!("{}/",&self.base_path))
        ];
        envs.append(&mut FnHttpService::headers_to_env(req.headers()));
        let stdin = req.collect().await?.to_bytes();

        // required because wasmtime calls "block_on" which only works if this method of called in a "block_in_place" block
        // https://docs.rs/tokio/latest/tokio/task/fn.block_in_place.html
        let result = tokio::task::block_in_place(move || {
            self.fn_manager.run_module(name, args.as_slice(), envs.as_slice(), stdin)
        });

        match result {
            Ok(Some(result)) => {
                let (headers, body) = FnHttpService::split_headers_and_body(&result);
                let mut response_builder = Response::builder().status(StatusCode::OK);
                for header in headers {
                    response_builder = response_builder.header(header.0, header.1);
                }
                Ok(response_builder.body(Full::from(body)).unwrap())
            },
            Ok(None) => {
                Ok(Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .header(header::CONTENT_TYPE, "text/plain")
                    .body(Full::from(format!("Function not found: {name}"))).unwrap())
            },
            Err(err) => Err(err)
        }
    }

    fn headers_to_env(headers: &HeaderMap<HeaderValue>) -> Vec<(String,String)> {
        let mut envs: Vec<(String,String)> = vec![];
        for header in headers {
            let value = header.1.to_str().unwrap_or("").to_string();
            if !value.is_empty() {
                let key = format!("X_HEADER_{}",header.0.to_string().to_ascii_uppercase().replace("-","_"));
                envs.push((key, value))
            }
        }
        envs
    }

    fn split_headers_and_body(content: &str) -> (Vec<(String,String)>, String) {
        let mut headers: Vec<(String,String)> = vec![];
        let mut body = "".to_string();
        let mut body_part = false;
        for line in content.lines() {
            if body_part {
                body.push_str(line);
                body.push_str("\n");
            } else if line.contains(":") {
                let mut splitter = line.splitn(2, ":");
                let key = splitter.next().unwrap();
                let value = splitter.next().or(Some("")).unwrap();
                headers.push((key.to_string(), value.to_string()));
            } else if line.trim().is_empty() {
                body_part = true;
            }
        }
        return (headers, body)
    }
}
