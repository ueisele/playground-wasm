fn main() {
    env_logger::init();

    futures::executor::block_on(async_main()).unwrap();
}

async fn async_main() -> anyhow::Result<()> {
    let addr = "127.0.0.1:8080".parse()?;

    let socket = tokio::net::TcpSocket::new_v4()?;
    socket.set_reuseaddr(true)?;
    socket.bind(addr)?;

    let listener = socket.listen(1024)?;

    log::info!("Serving HTTP on http://{}/", listener.local_addr()?);

    loop {
        match listener.accept().await {
            Ok((stream, _)) => {
                if let Err(err) = hyper::server::conn::http1::Builder::new()
                    .keep_alive(true)
                    .serve_connection(
                        hyper_util::rt::TokioIo::new(stream),
                        hyper::service::service_fn(move |req| handle_request(req))
                    )
                    .await {

                }
            }
            Err(err) => log::error!("{}", err)
        }
    }

    Ok(())
}

async fn handle_request(
    req: hyper::Request<hyper::body::Incoming>
) -> anyhow::Result<hyper::Response<http_body_util::StreamBody<http_body_util::BodyStream<http_body_util::Full<bytes::Bytes>>>>> {
    Err(anyhow::Error::msg(""))
}
