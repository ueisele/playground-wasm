#[cfg(test)]
mod tests_async {
    use std::time::Duration;
    use futures::executor::block_on;

    struct Song {
        name: String,
    }

    async fn learn_song(name: impl AsRef<str> + ToString) -> Song {
        for _ in 0..50 {
            println!("learning song '{}' ...", name.to_string());
            async_io::Timer::after(Duration::from_millis(50)).await;
        }
        Song { name: name.to_string() }
    }

    async fn sing_song(song: &Song) {
        for _ in 0..50 {
            println!("singing song '{}' ...", song.name.to_string());
            async_io::Timer::after(Duration::from_millis(50)).await;
        }
    }

    async fn dance() {
        for _ in 0..100 {
            println!("dancing ...");
            async_io::Timer::after(Duration::from_millis(50)).await;
        }
    }

    async fn learn_and_sing_song(name: impl AsRef<str> + ToString) {
        sing_song(&learn_song(name).await).await;
    }

    async fn async_main() {
        let learn_sing = learn_and_sing_song("Nothing else matters");
        let dance = dance();

        futures::join!(learn_sing, dance);
    }

    // run with '--nocapture' to print output during test run
    #[test]
    fn main() {
        block_on(async_main());
    }
}