#!/bin/sh

set -ex

cargo build --release

curl -Lo target/wasm32-wasip1/release/wasi_snapshot_preview1.proxy.wasm \
  https://github.com/bytecodealliance/wasmtime/releases/download/v19.0.0/wasi_snapshot_preview1.proxy.wasm

wasm-tools component new target/wasm32-wasip1/release/hello_wasi_http.wasm \
    --adapt target/wasm32-wasip1/release/wasi_snapshot_preview1.proxy.wasm \
    -o target/wasm32-wasip1/release/hello_wasi_http.component.wasm
