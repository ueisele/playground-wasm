= WebAssembly System Interface (WASI) Preview 2 ABI

https://github.com/WebAssembly/WASI/tree/main/preview2

* https://wasmcloud.com/blog/wasi-preview-2-officially-launches
* https://thenewstack.io/wasi-preview-2-a-new-dawn-for-webassembly/
