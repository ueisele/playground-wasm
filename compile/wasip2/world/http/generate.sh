#!/bin/sh

set -ex

generate() {
  wit-bindgen rust wit --out-dir src --std-feature "$@" --rustfmt \
    --bitflags-path bitflags \
    --runtime-path wit_bindgen_rt
}

mkdir -p src/

wit-deps

# Generate the main body of the bindings which includes all imports from the two
# worlds below.
generate --type-section-suffix rust-wasi-from-crates-io

# Generate bindings for the `wasi:http/proxy` world specifically, namely the
# macro `export_proxy`.
#
# Note that `--with` is used to point at the previously generated bindings.
with="wasi:cli/stdin@0.2.0=crate::cli::stdin"
with="$with,wasi:cli/stdout@0.2.0=crate::cli::stdout"
with="$with,wasi:cli/stderr@0.2.0=crate::cli::stderr"
with="$with,wasi:clocks/monotonic-clock@0.2.0=crate::clocks::monotonic_clock"
with="$with,wasi:clocks/wall-clock@0.2.0=crate::clocks::wall_clock"
with="$with,wasi:io/error@0.2.0=crate::io::error"
with="$with,wasi:io/poll@0.2.0=crate::io::poll"
with="$with,wasi:io/streams@0.2.0=crate::io::streams"
with="$with,wasi:random/random@0.2.0=crate::random::random"
with="$with,wasi:http/types@0.2.0=crate::http::types"
with="$with,wasi:http/outgoing-handler@0.2.0=crate::http::outgoing_handler"
generate \
  --world wasi:http/proxy \
  --with "$with" \
  --type-section-suffix rust-wasi-from-crates-io-proxy-world \
  --default-bindings-module wasi \
  --pub-export-macro \
  --export-macro-name _export_proxy