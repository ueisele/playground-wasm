use std::fs::File;
use std::io;

fn main() {
    let wasmtime_version = "v19.0.0";
    let adapter_files: Vec<&str> = vec![
        "wasi_snapshot_preview1.command.wasm",
        "wasi_snapshot_preview1.proxy.wasm",
        "wasi_snapshot_preview1.reactor.wasm"
    ];
    for adapter_file in adapter_files {
        let url = format!("https://github.com/bytecodealliance/wasmtime/releases/download/{wasmtime_version}/{adapter_file}");
        let target = format!("src/{}", adapter_file);
        download(&url, &target);
    }
}

fn download(url: &str, target: &str) {
    let mut resp = reqwest::blocking::get(url).expect(format!("request to {} failed", url).as_str());
    let mut out = File::create(target).expect(format!("failed to create target file {target}").as_str());
    io::copy(&mut resp, &mut out).expect(format!("failed to copy content from {url} to {target}").as_str());
}