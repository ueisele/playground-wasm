export function roll_dice(): i32 {
  return Math.ceil(Math.random() * 6) as i32
}
