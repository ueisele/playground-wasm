= AssemblyScript

* https://www.assemblyscript.org/introduction.html
* https://github.com/AssemblyScript/assemblyscript
* https://developer.fermyon.com/wasm-languages/assemblyscript
* https://github.com/AssemblyScript/assemblyscript

== WASM

[source,bash]
----
work/node_modules/assemblyscript/bin/asc.js fib.ts --outFile fib.wasm --optimize
----

[source,bash]
----
wasmtime --invoke fib fib.wasm 7
wasmer run --entrypoint fib fib.wasm 7
wasmedge fib.wasm fib 7
----

== WASI

In this example we make use of random numbers, which requires WASI.

Hint: We need a link:https://github.com/AssemblyScript/wasi-shim[WASI shim] for AssemblyScript which patches the AssemblyScript compiler to utilize WASI imports instead of Web APIs.

[source,bash]
----
mkdir work
cd work
npm install --save-dev assemblyscript
npm install --save-dev @assemblyscript/wasi-shim
cd ..
----

[source,bash]
----
work/node_modules/assemblyscript/bin/asc.js dice.ts --outFile dice.wasm --optimize --target release --config ./work/node_modules/@assemblyscript/wasi-shim/asconfig.json
----

[source,bash]
----
wasmtime --invoke roll_dice dice.wasm
----
