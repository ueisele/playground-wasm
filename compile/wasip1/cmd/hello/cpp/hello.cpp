#include <iostream>

using namespace std;

extern char **environ;
int main(int argc, char **argv) {
  cout << "Hello C++!" << endl << endl;

  cout << "==== Args: ====" << endl;
  do {
    cout << *argv << endl;
  } while(*++argv);

  cout << "==== Environment: ====" << endl;
  while(*environ) {
    cout << *environ++ << endl;
  }
}
