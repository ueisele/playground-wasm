console.log("Hello, Grain!\n")

console.log("==== Args: ====")
process.argv.forEach((v,i,self) => console.log(v))

console.log("==== Environment: ====")
process.env.keys().forEach((v,i,self) => console.log(v + "=" + process.env.get(v)))
