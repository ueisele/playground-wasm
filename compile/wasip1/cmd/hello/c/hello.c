#include <stdio.h>

extern char **environ;
int main(int argc, char **argv)
{
  printf("Hello C!\n\n");

  printf("==== Args: ====\n");
  do {
    printf("%s\n", *argv);
  } while(*++argv);

  printf("==== Environment: ====\n");
  while(*environ) {
    printf("%s\n", *environ++);
  }
    
  return 0;
}
