= Rust

* https://blog.dkwr.de/development/rust-wasm-compilation/
* https://github.com/bytecodealliance/wasmtime/blob/main/docs/WASI-tutorial.md
* https://wasmedge.org/docs/category/develop-wasm-apps-in-rust

.Prepare
[source,bash]
----
rustup target add wasm32-wasi
----

== Hello World

.Build
[source,bash]
----
rustc hello.rs --target wasm32-wasi
wasm-opt -Os -o hello.wasm hello.wasm
----

.Test
[source,bash]
----
echo "Hello World :)" | wasmtime --env hello=wasm hello.wasm foo=bar
echo "Hello World :)" | wasmedge --env hello=wasm hello.wasm foo=bar
wasmer --env hello=wasm hello.wasm foo=bar
----

== File Access

.Build
[source,bash]
----
rustc file.rs --target wasm32-wasi
wasm-opt -Os -o file.wasm file.wasm
----

.Test
[source,bash]
----
wasmtime file.wasm
----

----
Hello world!
thread 'main' panicked at file.rs:20:65:
called `Result::unwrap()` on an `Err` value: Custom { kind: Uncategorized, error: "failed to find a pre-opened file descriptor through which \"/helloworld/helloworld.txt\" could be opened" }
----

.Test
[source,bash]
----
mkdir target
wasmtime --dir target/::/helloworld file.wasm
----
