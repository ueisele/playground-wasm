use std::io::IsTerminal;

fn main() {
  println!("Hello Rust!\n");

  println!("==== Args: ====");
  std::env::args().for_each(|arg| println!("{}",arg));
  
  println!("==== Environment: ====");
  std::env::vars().for_each(|var| println!("{}={}",var.0,var.1));

  println!("==== Stdin: ====");
  if !std::io::stdin().is_terminal() {
    std::io::stdin().lines().for_each(|line| println!("{}", line.unwrap()))
  }
}
