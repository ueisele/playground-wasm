# Game of Life with WASM

Tutorial: https://rustwasm.github.io/docs/book/game-of-life/introduction.html

## 🚴 Usage

### 🐑 Use `cargo generate` to Clone this Template

[Learn more about `cargo generate` here.](https://github.com/ashleygwilliams/cargo-generate)

```bash
cargo generate --git https://github.com/rustwasm/wasm-pack-template.git --name my-project
cd my-project
```

### 🛠️ Build with `wasm-pack build`

```bash
wasm-pack build
```

### 🔬 Test in Headless Browsers with `wasm-pack test`

```bash
wasm-pack test --headless --firefox
```

### 🎁 Publish to NPM with `wasm-pack publish`

```bash
wasm-pack publish
```

## 🔋 Batteries Included

* [`wasm-bindgen`](https://github.com/rustwasm/wasm-bindgen) for communicating
  between WebAssembly and JavaScript.
* [`console_error_panic_hook`](https://github.com/rustwasm/console_error_panic_hook)
  for logging panic messages to the developer console.
