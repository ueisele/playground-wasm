import { Universe, Cell } from "wasm-game-of-life";
import { memory } from "wasm-game-of-life/wasm_game_of_life_bg.wasm";

const CELL_SIZE = 8; // px
const GRID_COLOR = "#CCCCCC";
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#000000";

const DEFAULT_WIDTH = 64
const DEFAULT_HEIGHT = 64

let universe;
let width;
let height;

const gameWidthInput = document.getElementById("game-width");
gameWidthInput.value = DEFAULT_WIDTH
const gameHeightInput = document.getElementById("game-height");
gameHeightInput.value = DEFAULT_HEIGHT

const canvas = document.getElementById("game-of-life-canvas");
const ctx = canvas.getContext('2d');
const headerDiv = document.getElementById("header-div");

let animationId = null;

let ticks_per_frame = 1.0
let frame_count = 0

const renderLoop = () => {
    if (Math.floor(frame_count % (1/ticks_per_frame)) === 0) {
        universe.tick();
        drawCells();
    }
    ++frame_count
    animationId = requestAnimationFrame(renderLoop);
};

const drawGrid = () => {
    ctx.beginPath();
    ctx.strokeStyle = GRID_COLOR;

    // Vertical lines.
    for (let i = 0; i <= width; i++) {
        ctx.moveTo(i * (CELL_SIZE + 1) + 1, 0);
        ctx.lineTo(i * (CELL_SIZE + 1) + 1, (CELL_SIZE + 1) * height + 1);
    }

    // Horizontal lines.
    for (let j = 0; j <= height; j++) {
        ctx.moveTo(0,                           j * (CELL_SIZE + 1) + 1);
        ctx.lineTo((CELL_SIZE + 1) * width + 1, j * (CELL_SIZE + 1) + 1);
    }

    ctx.stroke();
};

const getIndex = (row, column) => {
    return row * width + column;
};

const drawCells = () => {
    const cellsPtr = universe.cells();
    const cells = new Uint8Array(memory.buffer, cellsPtr, width * height);

    ctx.beginPath();

    for (let row = 0; row < height; row++) {
        for (let col = 0; col < width; col++) {
            const idx = getIndex(row, col);

            ctx.fillStyle = cells[idx] === Cell.Dead
                ? DEAD_COLOR
                : ALIVE_COLOR;

            ctx.fillRect(
                col * (CELL_SIZE + 1) + 1,
                row * (CELL_SIZE + 1) + 1,
                CELL_SIZE,
                CELL_SIZE
            );
        }
    }

    ctx.stroke();
};

canvas.addEventListener("click", event => {
    const boundingRect = canvas.getBoundingClientRect();

    const scaleX = canvas.width / boundingRect.width;
    const scaleY = canvas.height / boundingRect.height;

    const canvasLeft = (event.clientX - boundingRect.left) * scaleX;
    const canvasTop = (event.clientY - boundingRect.top) * scaleY;

    const row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), height - 1);
    const col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), width - 1);

    if (window.event.ctrlKey) {
        universe.toggle_cell(row, col);
        universe.toggle_cell(row + 1, col + 1);
        universe.toggle_cell(row + 2, col - 1);
        universe.toggle_cell(row + 2, col);
        universe.toggle_cell(row + 2, col + 1);
    } else {
        universe.toggle_cell(row, col);
    }

    drawCells();
});

const isPaused = () => {
    return animationId === null;
};

const playPauseButton = document.getElementById("play-pause");

const play = () => {
    playPauseButton.textContent = "⏸";
    renderLoop();
};

const pause = () => {
    playPauseButton.textContent = "▶";
    cancelAnimationFrame(animationId);
    animationId = null;
};

playPauseButton.addEventListener("click", event => {
    if (isPaused()) {
        play();
    } else {
        pause();
    }
});

const reset = (newWidth, newHeight) => {
    pause()
    if (universe != null) {
        universe.free()
    }

    universe = Universe.new_with_size_random(newWidth, newHeight)
    width = universe.width()
    height = universe.height()

    canvas.width = (CELL_SIZE + 1) * universe.width() + 1;
    canvas.height = (CELL_SIZE + 1) * universe.height() + 1;
    headerDiv.setAttribute("style",`display: flex; width:${canvas.width}px;`);

    drawGrid();
    drawCells();
}

const clear = (newWidth, newHeight) => {
    pause()
    if (universe != null) {
        universe.free()
    }

    universe = Universe.new_with_size_empty(newWidth, newHeight)
    width = universe.width()
    height = universe.height()

    canvas.width = (CELL_SIZE + 1) * universe.width() + 1;
    canvas.height = (CELL_SIZE + 1) * universe.height() + 1;
    headerDiv.setAttribute("style",`display: flex; width:${canvas.width}px;`);

    drawGrid();
    drawCells();
}

const resetButton = document.getElementById("reset");
resetButton.textContent = "⟳"
resetButton.addEventListener("click", event => {
    reset(gameWidthInput.value, gameHeightInput.value)
});

const clearButton = document.getElementById("clear");
clearButton.textContent = "🗑️"
clearButton.addEventListener("click", event => {
    clear(gameWidthInput.value, gameHeightInput.value)
});

const speedRange = document.getElementById("game-speed");
speedRange.addEventListener("input", (event) => {
    ticks_per_frame = event.target.value;
});

reset(DEFAULT_WIDTH, DEFAULT_HEIGHT)
